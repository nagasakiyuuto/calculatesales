package jp.alhinc.nagasaki_yuuto.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CalculateSales {
	public static void main(String[] args) {
		// コマンドラインから渡された引数の最初の文字列をargs[0]で呼び出す
		System.out.println("ここにあるファイルを開きます=>" + args[0]);

		// ハッシュマップを生成
		HashMap<String, String> branchmap = new HashMap<String, String>();

		// ディレクトリ検索結果を保持するリスト
		List<String> storage = new ArrayList<String>();

		BufferedReader br = null;
		BufferedReader br2 = null;

		try {
			// Fileクラスのオブジェクトを生成する(引数はディレクトリを指定)
			File filedirectory = new File(args[0]);
			File[] listdirectory = filedirectory.listFiles();
			System.out.println(listdirectory.length);

			// ディレクトリの名前を取得＆名前を検索して格納
			for (int i = 0; i < listdirectory.length; i++) {
				listdirectory[i].getName();
				// System.out.println(listdirectory[i]);
				if (listdirectory[i].getName().matches("^[0-9]{8}.rcd$")) {

					//storage.add(listdirectory[i].getName());
					//System.out.println(storage.get(i));
				}
			}

			// for文でlistdirectoryファイル読み込み
			for (int i = 0; i < listdirectory.length; i++) {
				FileReader fr2 = new FileReader(listdirectory[i]);
				br2 = new BufferedReader(fr2);
			}


			// Fileクラスのオブジェクト生成・バッファに保存する。
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 1行ごとに出力
			while ((line = br.readLine()) != null) {
				//System.out.println(line);
				// nullになるまでsplitで(",")わけ、わかれた値をmapに代入
				String[] branchlist = line.split(",");
				branchmap.put(branchlist[0], branchlist[1]);
				// System.out.println(branchmap.keySet());
			}
		} catch (IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}
}