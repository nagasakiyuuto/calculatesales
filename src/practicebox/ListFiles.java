package practicebox;

import java.io.File;

public class ListFiles {

	    public static void main(String[] args) {
	        //Fileクラスのオブジェクトを生成する
	        File dir = new File(args[0],"branch.lst");

	        //listFilesメソッドを使用して一覧を取得する
	        File[] list = dir.listFiles();

	        System.out.println(list.length);
	    }

	}